###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01e - Crazy Cat Lady - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Makefile for lab01e Crazy Cat Lady
### 
### compile: g++ -o crazyCatLady crazyCatLady.cpp
###  
### @author  Arthur Lee <leea3@hawaii.edu>
### @date    13 Jan 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = g++
CFLAGS = -g -Wall

TARGET = crazyCatLady

all: $(TARGET)

crazyCatLady: crazyCatLady.cpp
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatLady.cpp

clean:
	rm -f $(TARGET) *.o

